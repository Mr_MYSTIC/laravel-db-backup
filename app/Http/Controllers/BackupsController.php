<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use DB;
use Config;

class BackupsController extends Controller
{
    private $remoteDB = null;
    private $maxInsertRows = 5000;
    private $cheatCodeValue = 'q9cFkd1qS6u1h59YrFTp';

    private $stateFile = null;

    public function __construct()
    {
        ini_set('max_execution_time', 10); // just for emulate hard restrictions

        $this->stateFile = implode(DIRECTORY_SEPARATOR, [
            storage_path(),
            'state.json',
        ]);
    }

    /**
     * @param string $value
     */
    public function getCheatCode($value) {
        if ($value === $this->cheatCodeValue) {
            if ($this->functionAvailable('exec')) {

                $filename = 'backup-' . date('Ymd_His') . '.sql';

                $password = Config::get('database.connections.mysql.password');
                $cmd = 'mysqldump ';
                $cmd .= '-h' . Config::get('database.connections.mysql.host') . ' ';
                $cmd .= '-P' . Config::get('database.connections.mysql.port') . ' ';
                $cmd .= '-u' . Config::get('database.connections.mysql.username') . ' ';
                if (!empty($password)) {
                    $cmd .= '-p' . $password . ' ';
                }
                $cmd .= Config::get('database.connections.mysql.database') . ' > ' . implode(DIRECTORY_SEPARATOR, [
                        storage_path(),
                        'backups',
                        $filename
                    ]);
                echo nl2br(exec($cmd));
            }
        }
    }

    /**
     * @return View
     */
    public function getProcess()
    {
        try {
            $data = $this->restoreState();
        } catch (\Exception $e) {
            return redirect()->route('home');
        }

        Config::set('database.connections.temp', $data['connection']);
        $file = $data['file'];

        $this->remoteDB = DB::connection('temp');

        foreach ($data['tables'] as $tableName => &$tableData) {

            if ($tableData['count'] == $tableData['processed']) {
                continue;
            }

            if (!$tableData['drop_create_added']) {
                $drop = "DROP TABLE IF EXISTS `$tableName`;\n\n";
                $this->appendBackUp($file, $drop);
                $create = $this->remoteDB->select("SHOW CREATE TABLE `{$tableName}`;")[0]->{'Create Table'} . ";\n\n";
                $this->appendBackUp($file, $create);
                $tableData['drop_create_added'] = true;

                $this->saveState($data);
            }

            if (!$tableData['locked']) {
                $this->remoteDB->raw("LOCK TABLES `{$tableName}` WRITE;");
                $tableData['locked'] = true;
                $this->saveState($data);
            }

            $portion = $this->remoteDB->table($tableName)
                ->where($tableData['primary_key'], '>', $tableData['last_id'])
                ->orderBy($tableData['primary_key'])
                ->take($this->maxInsertRows)
                ->get()
                ->toArray();

            if (count($portion) === 0) {
                $this->remoteDB->raw("UNLOCK TABLES;");
                $tableData['locked'] = false;
                $this->saveState($data);
                return view('backups.process', compact('data'));
            }

            $headers = false;
            $values = [];

            foreach ($portion as $item) {
                $item = json_decode(json_encode($item), true);

                if ($headers === false) {
                    $headers = array_keys($item);
                }

                $values[] = "('" . implode("', '", array_values($item)) . "')";
            }

            $insertPart = $this->getInsertQuery($tableName, $headers, $values);
            $appended = $this->appendBackUp($file, $insertPart);

            if ($appended !== false) {

                $last = end($portion);
                $tableData['last_id'] = $last->{$tableData['primary_key']};

                $tableData['processed'] += count($portion);
                $tableData['progress'] = round($tableData['processed'] / $tableData['count'] * 100);

                $data['processed'] += count($portion);
                $data['progress'] = round($data['processed'] / $data['count'] * 100);

                $this->saveState($data);

                if ($data['processed'] === $data['count']) {
                    unlink($this->stateFile);
                }

                $this->remoteDB->disconnect('temp');
                return view('backups.process', compact('data'));
            }
        }

        $this->remoteDB->disconnect('temp');
        return view('backups.process', compact('data'));
    }

    /**
     * @param Request $request
     * @return View
     */
    public function postStart(Request $request)
    {
        $defaults = [
            'driver' => 'mysql',
            'host' => Config::get('database.connections.mysql.host'),
            'port' => Config::get('database.connections.mysql.port'),
            'database' => Config::get('database.connections.mysql.database'),
            'username' => Config::get('database.connections.mysql.username'),
            'password' => Config::get('database.connections.mysql.password'),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ];

        $connectionData = array_merge($defaults, $request->except('_token'));

        Config::set('database.connections.temp', $connectionData);

        $this->remoteDB = DB::connection('temp');

        $tables = $this->remoteDB->select("SHOW TABLES;");

        $data = [
            'tables' => [],
            'count' => 0,
            'processed' => 0,
            'progress' => 0,
            'file' => implode(DIRECTORY_SEPARATOR, [
                storage_path(),
                'backups',
                'backup-' . date('Ymd_His') . '.sql',
            ]),
            'connection' => $connectionData,
        ];

        foreach ($tables as $table) {
            $key = 'Tables_in_' . $connectionData['database'];
            $tableName = $table->{$key};

            $count = $this->remoteDB->table($tableName)->count();
            $tableData = $this->remoteDB->select("DESCRIBE `{$tableName}`;");

            $primaryKey = null;
            foreach ($tableData as $field) {
                if ($field->Key == 'PRI') {
                    $primaryKey = $field->Field;
                }
            }

            $data['tables'][$tableName] = [
                'count' => $count,
                'processed' => 0,
                'progress' => 0,
                'drop_create_added' => false,
                'last_id' => 0,
                'primary_key' => $primaryKey,
                'locked' => false,
            ];

            $data['count'] += $count;
        }

        $this->saveState($data);

        $this->remoteDB->disconnect('temp');

        return redirect()->route('backup_process');
    }

    /**
     * @param string $functionName
     * @return bool
     */
    private function functionAvailable($functionName)
    {
        $disabled = explode(',', ini_get('disable_functions'));

        return function_exists($functionName) && !in_array($functionName, $disabled);
    }

    /**
     * @param array $data
     */
    private function saveState($data)
    {
        $data = json_encode($data);
        file_put_contents($this->stateFile, $data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function restoreState()
    {
        if (!file_exists($this->stateFile)) {
            throw new \Exception();
        }

        $data = file_get_contents($this->stateFile);
        return json_decode($data, true);
    }

    /**
     * @param $file
     * @param $data
     */
    private function appendBackUp($file, $data)
    {
        file_put_contents($file, $data, FILE_APPEND);
    }

    /**
     * @param $table
     * @param $headers
     * @param $values
     * @return string
     */
    private function getInsertQuery($table, $headers, $values)
    {
        $headers = '`' . implode('`, `', $headers) . '`';
        $values = implode(",\n", $values);

        return "INSERT INTO `{$table}` ({$headers}) VALUES\n {$values};\n\n";
    }
}
