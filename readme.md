# Laravel application for database backup

## Installation

- `mkdir laravel-db-backup`
- `cd laravel-db-backup`
- `git clone ssh://git@bitbucket.org/Mr_MYSTIC/laravel-db-backup.git .` 
- `cp .env.example .env`
- `composer install`

## Using 

- Open in browser /public
- Type database credentials
- Submit form

## Backup process

- After form submission application will check database tables and will count entries
- Then application will serialize data and save to temp file
- Next step is redirecting to the process route
- Process method will read the temp file and unserialize data
- For each table application will select from database 5000 entries and will create sql query
- When the entries will be saved application will redirect browser to the process route until all entries from all tables will be saved as sql queries
- When all data saved the temp file will be removed