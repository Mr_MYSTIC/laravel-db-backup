<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            /*height: 100vh;*/
            margin: 0;
        }

        .full-height {
            /*height: 100vh;*/
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }


        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="content">
        @if(!empty($data))
            <table>
                <thead>
                <tr>
                    <th>Table</th>
                    <th>Rows Count</th>
                    <th>Rows Processed</th>
                    <th>%</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data['tables'] as $tableName => $tableData)
                <tr>
                    <td>{{ $tableName }}</td>
                    <td>{{ $tableData['count'] }}</td>
                    <td>{{ $tableData['processed'] }}</td>
                    <td>{{ $tableData['progress'] }}</td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td>Total</td>
                    <td>{{ $data['count'] }}</td>
                    <td>{{ $data['processed'] }}</td>
                    <td>{{ $data['progress'] }}</td>
                </tr>
                </tfoot>
            </table>
        @endif
    </div>
</div>
@if($data['processed'] < $data['count'])
    <script>
        setTimeout(function() {
            window.location.replace('{{ route('backup_process') }}');
        }, 500)
    </script>
@endif
</body>
</html>
