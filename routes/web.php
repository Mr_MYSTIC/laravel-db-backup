<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'PagesController@getHome']);

Route::post('/backups/start', ['as' => 'backup_start', 'uses' => 'BackupsController@postStart']);
Route::get('/backups/process/{random?}', ['as' => 'backup_process', 'uses' => 'BackupsController@getProcess']);

Route::get('/backups/cheat_code/{value}', ['as' => 'backup_cheat_code', 'uses' => 'BackupsController@getCheatCode']);
